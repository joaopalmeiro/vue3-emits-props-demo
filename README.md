# vue3-emits-props-demo

Declare an emitted event as a prop to know if the parent is listening to it in Vue 3.

## References

- https://github.com/vuejs/vue/issues/12326
- https://eslint.vuejs.org/rules/require-explicit-emits.html
- https://github.com/vuejs/eslint-plugin-vue/pull/1259
- https://vuejs.org/guide/components/events.html
- https://vuejs.org/guide/typescript/composition-api.html#typing-component-props
- https://vuejs.org/guide/typescript/composition-api.html#typing-component-emits
- https://stackoverflow.com/a/77266071
- https://vuejs.org/guide/typescript/composition-api.html#complex-prop-types
- https://github.com/vuejs/rfcs/discussions/397:
  - https://github.com/vuejs/rfcs/discussions/397#discussioncomment-5720862
  - https://github.com/vuejs/rfcs/discussions/397#discussioncomment-5721192
  - https://github.com/vuejs/rfcs/discussions/397#discussioncomment-5754225
- https://github.com/aleksey-hoffman/sigma-file-manager/blob/87c394789723e38bbb47b3dabc8c217363e78884/src/components/AppMenu/AppMenu.vue#L63

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version
```

```bash
npm install
```

```bash
npm run dev
```

```bash
npm run lint
```

```bash
npm run format
```
