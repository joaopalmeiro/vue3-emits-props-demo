# Notes

- https://github.com/ota-meshi/eslint-plugin-vue-demo
- https://vuejs.org/api/sfc-css-features.html#v-bind-in-css
- https://github.com/frenic/csstype
- https://github.com/mdn/data
- https://github.com/mdn/data/blob/main/css/properties.json
- https://github.com/MasterSais/css-enums
- https://github.com/MasterSais/css-enums/blob/master/index.d.ts#L3569
- https://github.com/vuejs/core/blob/v3.3.8/packages/runtime-core/src/componentEmits.ts#L96
- https://github.com/vuejs/core/blob/v3.3.8/packages/shared/src/general.ts#L124
- `[Vue warn]: Component emitted event "onCustomEvent" but it is neither declared in the emits option nor as an "onOnCustomEvent" prop.`
- https://vuejs.org/guide/components/events.html#emitting-and-listening-to-events

## Commands

```bash
npm install vue && npm install -D vite @vitejs/plugin-vue typescript vue-tsc create-vite-tsconfigs sort-package-json npm-run-all2 prettier
```

```bash
rm -rf node_modules/ && npm install
```
